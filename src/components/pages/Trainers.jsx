import React from 'react';
import "./trainers.css";
import testi7 from "../tasbir/testi7.jpg";
import testi6 from "../tasbir/testi6.jpg";
import testi8 from "../tasbir/testi8.jpg";
import testi4 from "../tasbir/testi4.jpg";
import gym3 from "../tasbir/gym3.jpg";
import cus3 from "../tasbir/cus3.jpg";
import cus1 from "../tasbir/cus1.jpg";
import cus5 from "../tasbir/cus5.jpg";
import { FaFacebook, FaTwitter, FaTelegram, FaLinkedin } from "react-icons/fa";
import Slider4 from '../inc/Slider4';



function Trainers() {
    return (
        <>
            <Slider4 />

            <section className="section border-top">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 mb-3 text-center">
                            <h3 className="main-heading color-danger">OUR TRAINERS</h3>
                            <div className="underline mx-auto"></div>
                        </div>
                        <div className="col-md-3 shadow ">
                            <div className="card">

                                <img id="photo" src={testi6} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>MR.JAAN</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Trainer</p>
                                    <i><FaFacebook /></i>
                                    <i><FaTelegram /></i>
                                    <i><FaTwitter /></i>
                                    <i><FaLinkedin /></i>


                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 shadow">
                            <div className="card">

                                <img id="photo" src={testi7} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>MR.JOJO</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Trainer</p>
                                    <i><FaFacebook /></i>
                                    <i><FaTelegram /></i>
                                    <i><FaTwitter /></i>
                                    <i><FaLinkedin /></i>

                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 shadow">
                            <div className="card">

                                <img id="photo" src={testi8} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>MS.LELA</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Trainer</p>
                                    <i><FaFacebook /></i>
                                    <i><FaTelegram /></i>
                                    <i><FaTwitter /></i>
                                    <i><FaLinkedin /></i>

                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 shadow ">
                            <div className="card">

                                <img id="photo" src={testi4} className="w-100 h-100 border-buttom" alt="pic" />
                                <div className="card-body">
                                    <h6>MS.KATHRIN</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Trainer</p>
                                    <i><FaFacebook /></i>
                                    <i><FaTelegram /></i>
                                    <i><FaTwitter /></i>
                                    <i><FaLinkedin /></i>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </section>

            <section className="section border-top">
                <div className="container">
                    <div className="row">

                        <div className="col-md-3 shadow ">
                            <div className="card">

                                <img id="photo" src={gym3} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>MS.LELIN</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Trainer</p>
                                    <i><FaFacebook /></i>
                                    <i><FaTelegram /></i>
                                    <i><FaTwitter /></i>
                                    <i><FaLinkedin /></i>


                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 shadow">
                            <div className="card">

                                <img id="photo" src={cus3} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>MR.JORG</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Trainer</p>
                                    <i><FaFacebook /></i>
                                    <i><FaTelegram /></i>
                                    <i><FaTwitter /></i>
                                    <i><FaLinkedin /></i>

                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 shadow">
                            <div className="card">

                                <img id="photo" src={cus1} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>MS.SANEM</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Trainer</p>
                                    <i><FaFacebook /></i>
                                    <i><FaTelegram /></i>
                                    <i><FaTwitter /></i>
                                    <i><FaLinkedin /></i>

                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 shadow ">
                            <div className="card">

                                <img id="photo" src={cus5} className="w-100 h-100 border-buttom" alt="pic" />
                                <div className="card-body">
                                    <h6>MR.NEON</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Trainer</p>
                                    <i><FaFacebook /></i>
                                    <i><FaTelegram /></i>
                                    <i><FaTwitter /></i>
                                    <i><FaLinkedin /></i>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </section>

        </>
    );
}
export default Trainers;