import React from 'react';
import "./testimonials.css";
import { FaFacebook, FaTwitter, FaTelegram, FaLinkedin, FaQuoteLeft, FaQuoteRight } from "react-icons/fa";
import cus8 from "../tasbir/cus8.jpg";
import cus2 from "../tasbir/cus2.jpg";
import cus7 from "../tasbir/cus7.jpg";
import cus1 from "../tasbir/cus1.jpg";
import cus5 from "../tasbir/cus5.jpg";
import cus3 from "../tasbir/cus3.jpg";
import Slider5 from '../inc/Slider5';




function Testimonials() {
    return (
        <>
            <Slider5 />
            <section className="section ">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 text-center">
                            <h2 className="text-danger">OUR TESTIMONIALS</h2>
                            <div className="underline mx-auto"></div>
                        </div>
                    </div>
                </div>
            </section>


            <section >

                <div className="wrapper">
                    <div className="container">
                        <div className="profile">
                            <div className="imgBox">
                                <img src={cus2} alt="error" />
                            </div>
                            <h2>Person1</h2>
                        </div>
                        <p><span ><FaQuoteLeft /></span>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas, porro?<span >< FaQuoteRight /></span></p>
                        <div className="social">
                            <i><FaFacebook /></i>
                            <i ><FaTwitter /></i>
                            <i ><FaTelegram /></i>
                            <i ><FaLinkedin /></i>

                        </div>
                    </div>
                    <div className="container">
                        <div className="profile">
                            <div className="imgBox">
                                <img src={cus1} alt="error" />
                            </div>
                            <h2>Person2</h2>
                        </div>
                        <p><span ><FaQuoteLeft /></span>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas, porro?<span >< FaQuoteRight /></span></p>
                        <div className="social">
                            <i><FaFacebook /></i>
                            <i ><FaTwitter /></i>
                            <i ><FaTelegram /></i>
                            <i ><FaLinkedin /></i>

                        </div>
                    </div>
                    <div className="container">
                        <div className="profile">
                            <div className="imgBox">
                                <img src={cus7} alt="error" />
                            </div>
                            <h2>Person3</h2>
                        </div>
                        <p><span ><FaQuoteLeft /></span>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas, porro?<span >< FaQuoteRight /></span></p>
                        <div className="social">
                            <i><FaFacebook /></i>
                            <i ><FaTwitter /></i>
                            <i ><FaTelegram /></i>
                            <i ><FaLinkedin /></i>

                        </div>
                    </div>
                    <div className="container">
                        <div className="profile">
                            <div className="imgBox">
                                <img src={cus8} alt="error" />
                            </div>
                            <h2>Person4</h2>
                        </div>
                        <p><span ><FaQuoteLeft /></span>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas, porro?<span >< FaQuoteRight /></span></p>
                        <div className="social">
                            <i><FaFacebook /></i>
                            <i ><FaTwitter /></i>
                            <i ><FaTelegram /></i>
                            <i ><FaLinkedin /></i>

                        </div>
                    </div>
                    <div className="container">
                        <div className="profile">
                            <div className="imgBox">
                                <img src={cus5} alt="error" />
                            </div>
                            <h2>Person5</h2>
                        </div>
                        <p><span ><FaQuoteLeft /></span>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas, porro?<span >< FaQuoteRight /></span></p>
                        <div className="social">
                            <i><FaFacebook /></i>
                            <i ><FaTwitter /></i>
                            <i ><FaTelegram /></i>
                            <i ><FaLinkedin /></i>

                        </div>
                    </div>
                    <div className="container">
                        <div className="profile">
                            <div className="imgBox">
                                <img src={cus3} alt="error" />
                            </div>
                            <h2>Person6</h2>
                        </div>
                        <p><span ><FaQuoteLeft /></span>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptas, porro?<span >< FaQuoteRight /></span></p>
                        <div className="social">
                            <i><FaFacebook /></i>
                            <i ><FaTwitter /></i>
                            <i ><FaTelegram /></i>
                            <i ><FaLinkedin /></i>

                        </div>
                    </div>

                </div>

            </section>


        </>
    );
}
export default Testimonials;