import React from 'react';

function Vmc() {
    return (
        <section className="section bg-c-light border-top mt-3 pt-2">
            <div className="container">
                <div className="row">
                    <div className="col-md-12 mb-3 text-center shadow">
                        <h1 className="main-heading bg-dark text-white">vision mission and values</h1>

                    </div>
                    <div className="col-md-4 text-center shadow">
                        <h6 >our vision</h6>
                        <div className="underline mx-auto"></div>
                        <p className="text-dark">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Iusto repellendus hic corporis at provident, ipsum qui dolor possimus veniam molestias. Lorem ipsum dolor, sit amet consectetur adipisicing elit. Harum, quasi!
                        </p>

                    </div>
                    <div className="col-md-4 text-center shadow">
                        <h6>our mission</h6>
                        <div className="underline mx-auto"></div>
                        <p className="text-dark">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Fuga suscipit eligendi eum quae a dolor. Amet ipsam quos earum, expedita sapiente corporis vitae non, et laborum maiores perferendis ducimus velit.
                        </p>
                    </div>
                    <div className="col-md-4 text-center shadow">
                        <h6>our core values</h6>
                        <div className="underline mx-auto"></div>
                        <p className="text-dark">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam quod ipsum adipisci perferendis impedit quos labore mollitia voluptatem nesciunt dolores, consectetur aut quo, fugiat vel! Adipisci suscipit commodi tempore ducimus.
                        </p>
                    </div>
                </div>
            </div>
        </section>
    );
}
export default Vmc;