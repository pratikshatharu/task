/*import '../inc/contact.css'
import React, { useState } from 'react';
import Slider3 from '../inc/Slider3';

const Contact = () => {
    const [data, setData] = useState({
        fullname: '',
        email: '',
        phone: '',
        message: '',
    });
    const InputEvent = (event) => {
        const { name, value } = event.target;
        setData((preval) => {
            return {
                ...preval,
                [name]: value,
            }
        })
    };
    const formSubmit = (e) => {
        e.preventDefault();
        alert(`${data.fullname}`);

    };

    return (
        <>
            <Slider3 />
            <section className="section ">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 text-center bg-dark  shadow">
                            <h2 className="contact mt-1 text-white ">GET IN TOUCH</h2>
                            <div className="underline mx-auto"></div>

                        </div>


                    </div>
                </div>
            </section>
            <section className="mb-3">
                <div className="container contact_div">
                    <div className="row">
                        <div className="col-md-6 col-10 mx-auto">
                            <form onSubmit={formSubmit} className="shadow">
                                <div class="cl">
                                    <label for="label" class="form-label">Full Name</label>
                                    <input type="text" class="form-control shadow" name="fullname" value={data.fullname} onChange={InputEvent} placeholder="Enter your name"  />
                                </div>
                                <div class="cl">
                                    <label for="label" class="form-label">Email</label>
                                    <input type="email" class="form-control shadow" name="email" value={data.email} onChange={InputEvent} placeholder="Email@gmail.com" required />
                                </div>
                                <div class="cl">
                                    <label for="label" class="form-label">Phone</label>
                                    <input type="number" class="form-control shadow" name="phone" value={data.phone} onChange={InputEvent} placeholder="Mobile number" required />
                                </div>

                                <div class="cl">
                                    <label for="label" class="form-label">Message</label>
                                    <textarea class="form-control" name="message" value={data.message} onChange={InputEvent} rows="3" placeholder="Type your message..." required></textarea>
                                </div>

                                <div class="col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="invalidCheck2" required />
                                        <label class="form-check-label" for="invalidCheck2">
                                            Agree to terms and conditions
                                        </label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="submit" className="btn btn-outline-primary mb-3" >Submit</button>
                                </div>
                            </form>

                        </div>

                    </div>

                </div>
            </section>
        </>

    );
}
export default Contact;*/


import '../inc/contact.css'
import React, { useState } from 'react';
import Slider3 from '../inc/Slider3';



    function Contact() {
        const [user,setUser]=useState("");
        const [email,setEmail]=useState("");
        const [phone,setPhone]=useState("");
        const [message,setMessage]=useState("");
        const [userErr,setUserErr]=useState(false);
        const [emailErr,setEmailErr]=useState(false);
        const [phoneErr,setPhoneErr]=useState(false);
        const [messageErr,setMessageErr]=useState(false);
    
      function loginHandle(e)
        {
            if(user.length<3 || email.length<5 || phone.length<9 || message.length<15 )
            {
                alert("type correct values")
            }
            else
            {
                alert("Your message is submited :) Thank u for visiting")
            }
    
            e.preventDefault()
        }
        function userHandler(e){
            let item=e.target.value;
            if(item.length<3 )
            {
               setUserErr(true)
            }
            else
            {
                setUserErr(false)
            }
            setUser(item)
        }
        function emailHandler(e){
            let item=e.target.value;
            if(item.length<3 )
            {
               setEmailErr(true)
            }
            else
            {
                setEmailErr(false)
            }
            setEmail(item)
    
        }
       
        function phoneHandler(e){
            let item=e.target.value;
            if(item.length<9 )
            {
               setPhoneErr(true)
            }
            else
            {
                setPhoneErr(false)
            }
            setPhone(item)
    
        }
        function messageHandler(e){
            let item=e.target.value;
            if(item.length<15 )
            {
               setMessageErr(true)
            }
            else
            {
                setMessageErr(false)
            }
            setMessage(item)
    
        }

    return (
        <>
            <Slider3/>
            <section className="section ">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 text-center bg-dark  shadow">
                            <h2 className="contact mt-1 text-white ">GET IN TOUCH</h2>
                            <div className="underline mx-auto"></div>

                        </div>


                    </div>
                </div>
            </section>
            <section className="mb-3">
                <div className="container contact_div">
                    <div className="row">
                        <div className="col-md-6 col-10 mx-auto">
                            <form onSubmit={loginHandle} className="shadow">
                                <div class="cl">
                                    <label for="label" class="form-label">Full Name</label>
                                    <input type="text" class="form-control shadow" name="fullname"  onChange={userHandler} placeholder="Enter your name" required />{userErr?<span>Name Not Valid</span>:""}
                                </div>
                                <div class="cl">
                                    <label for="label" class="form-label">Email</label>
                                    <input type="email" class="form-control shadow" name="email"  onChange={emailHandler} placeholder="Email@gmail.com" required />{emailErr?<span>Email Not Valid</span>:""}
                                </div>
                                <div class="cl">
                                    <label for="label" class="form-label">Phone</label>
                                    <input type="number" class="form-control shadow" name="phone"  onChange={phoneHandler} placeholder="Mobile number" required />{phoneErr?<span>please enter more then 9 number.</span>:""}
                                </div>

                                <div class="cl">
                                    <label for="label" class="form-label">Message</label>
                                    <textarea class="form-control" name="message"  onChange={messageHandler} rows="3" placeholder="Type your message..." required></textarea>{messageErr?<span>Please write minimum 15 letters.</span>:""}
                                </div>

                                <div class="col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="invalidCheck2" required />
                                        <label class="form-check-label" for="invalidCheck2">
                                            Agree to terms and conditions
                                        </label>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="submit" className="btn btn-outline-primary mb-3" >Submit</button>
                                </div>
                            </form>

                        </div>

                    </div>

                </div>
            </section>
        </>

    );
}
export default Contact;

