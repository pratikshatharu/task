import React from 'react';
import Slider from '../inc/Slider';
import { Link } from "react-router-dom";
import Vmc from './inc/Vmc';
import slide2 from "../tasbir/slide2.jpg";
import slide3 from "../tasbir/slide3.jpg";
import slider4 from "../tasbir/slider4.jpg";
import slide1 from "../tasbir/slide1.jpg";
import "../pages/inc/home.css";

function Home() {
    return (
        <>
            <Slider />


            {/*vision mission and values */}
            <Vmc />
            {/*Services */}
            <section className="section border-top">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 mb-3 text-center">
                            <h1 className="main-heading">Our Services</h1>
                            <div className="underline mx-auto"></div>
                        </div>
                        <div className="col-md-3 shadow ">
                            <div className="card">

                                <img id="photo" src={slide2} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>Our Equipments</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">We are using high quality of equipments for the excersice. We give our best to the our clients.                             </p>
                                    <Link to="/about" >Read More!</Link>
                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 shadow">
                            <div className="card">

                                <img id="photo" src={slide3} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>Personal Training</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">You can think of personal training like our secret sauce – it’s the fastest and safest way to get seriously awesome results.</p>
                                    <Link to="/about" >Read More!</Link>
                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 shadow">
                            <div className="card">

                                <img id="photo" src={slide1} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>Small Group Training</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark"> Get sweaty with your best friend or make a new workout buddy on the turf in our group training sessions!</p>
                                    <Link to="/about" >Read More!</Link>
                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 shadow ">
                            <div className="card">

                                <img id="photo" src={slider4} className="w-100 h-100 border-buttom" alt="pic" />
                                <div className="card-body">
                                    <h6>Family Environment</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">We provide family environment t both of the staff member and our customers. </p>
                                    <Link to="/about" >Read More!</Link>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </section>

        </>
    );
}
export default Home;