import React from 'react';
import Slider2 from '../inc/Slider2';
import Vmc from './inc/Vmc';
import "../pages/about.css";
import con1 from "../tasbir/con1.jpg";
import { Link } from 'react-router-dom';



function About() {
    return (
        <>
            <Slider2 />
            <section className="section bg-light border-buttom">
                <div className="container text-center ">
                    <h2 className="main-heading ">FitFactory</h2>
                    <div className="underline mx-auto"></div>
                    <div className="row">
                        <div className="col-md-6">
                            <img src={con1} alt="error" />

                        </div>
                        <div className="col-md-6  text-white">
                            <h1 className="heading text-dark">ABOUT US</h1>
                            <p className="bg-dark text-white " >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam nisi nobis dolores. Illum soluta quisquam, consectetur eaque provident vitae atque voluptatibus ipsam alias dicta iusto facere sequi, consequatur quo, expedita quae quasi. Quia est aperiam repellat minus officiis delectus hic, iste, enim vero sed sequi nihil nesciunt velit animi voluptatum aliquid debitis consequuntur totam? Et nobis fugiat architecto reprehenderit quis.
                                <Link to="/about">READ MORE!</Link> </p>


                        </div>

                    </div>

                </div>
            </section>
            {/* for Vmc */}
            <Vmc />

        </>
    );
}
export default About;