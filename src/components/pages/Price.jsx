import React from 'react';
import Slider6 from '../inc/Slider6';
import gym6 from '../tasbir/gym6.jpg';
import jumba from '../tasbir/jumba.jpg';
import yoga1 from '../tasbir/yoga1.jpg';
import strength1 from '../tasbir/strength1.jpg';
import body from '../tasbir/body.jpg';
import work1 from '../tasbir/work1.jpg';
import pushups1 from '../tasbir/pushups1.jpg';
import balance from '../tasbir/balance.jpg';
import aerobic from '../tasbir/aerobic.jpg';
import { Link } from 'react-router-dom';



function Price() {
    return (
        <>
            <Slider6 />


            <section className="section bg-light border-buttom">
                <div className="container text-center ">
                    <div className="row">
                        <div className="col-md-6">
                            <img src={gym6} alt="error" />

                        </div>
                        <div className="col-md-6  text-white">
                            <h1 className="heading text-dark">START YOUR TRAINING TODAY</h1>
                            <p className="bg-dark text-white " >
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                                Nisi ad odit molestiae quaerat. Perferendis dolorum quod dol
                                oribus, quia mollitia. Lorem ipsum dolor sit amet consectetur
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aut
                                em, rem.adipisicing elit. Inventore, maiores.
                            </p>
                            <Link to="/contact" className="btn btn-danger">START NOW</Link>
                        </div>
                    </div>
                </div>
            </section>

            <section className="section border-top">
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 mb-3 text-center">
                            <h3 className="main-heading color-danger">OUR COURSES</h3>
                            <div className="underline mx-auto"></div>
                        </div>
                        <div className="col-md-3 shadow ">
                            <div className="card">

                                <img id="photo" src={jumba} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>$50  JUMBA</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Morning 6-7AM</p>
                                    <Link to="/contact" className="btn btn-danger">JOIN NOW</Link>


                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 shadow ">
                            <div className="card">

                                <img id="photo" src={body} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>$500 BODY BUILDING</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Morning 7-8AM</p>
                                    <Link to="/contact" className="btn btn-danger">JOIN NOW</Link>


                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 shadow ">
                            <div className="card">

                                <img id="photo" src={work1} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6> $250 WORKOUT</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Morning 8-9AM</p>
                                    <Link to="/contact" className="btn btn-danger">JOIN NOW</Link>

                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 shadow ">
                            <div className="card">

                                <img id="photo" src={yoga1} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>$450 YOGA</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Morning 5-6 AM</p>
                                    <Link to="/contact" className="btn btn-danger">JOIN NOW</Link>


                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 mt-3 shadow ">
                            <div className="card">

                                <img id="photo" src={aerobic} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6> $500 AEROBIC EXERCISE</h6>
                                    <div className="underline mb-1"></div>
                                    <p className="text-dark">Morning 10-11AM</p>
                                    <Link to="/contact" className="btn btn-danger">JOIN NOW</Link>


                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 mt-3 shadow ">
                            <div className="card">

                                <img id="photo" src={strength1} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>$1000 STRENGTH TRAINING</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Evening 5-6 PM</p>
                                    <Link to="/contact" className="btn btn-danger">JOIN NOW</Link>


                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 mt-3 shadow ">
                            <div className="card">

                                <img id="photo" src={balance} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>$1500 BALANCE EXCERCISE</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Evening 7-8PM</p>
                                    <Link to="/contact" className="btn btn-danger">JOIN NOW</Link>


                                </div>
                            </div>

                        </div>
                        <div className="col-md-3 mt-3 shadow ">
                            <div className="card">

                                <img id="photo" src={pushups1} className="w-100 h-100 border-buttom" alt="error" />
                                <div className="card-body">
                                    <h6>$2000 PUSH UPS</h6>
                                    <div className="underline  mb-1 "></div>
                                    <p className="text-dark">Evening 6-7 PM</p>
                                    <Link to="/contact" className="btn btn-danger">JOIN NOW</Link>


                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </section>

        </>

    );
}
export default Price;