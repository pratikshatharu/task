import React from 'react';
import './footer.css';
import { Link } from "react-router-dom";
import { FaFacebook, FaTwitter, FaTelegram, FaLinkedin } from "react-icons/fa";


function Footer() {
    return (
        <section className="section footer bg-dark text-white mx-auto">
            <div className="container">
                <div className="row">
                    <div className="col-md-4">
                        <h6>more information</h6>
                        <hr />
                        <p className="text-white">Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            Neque magnam tempore ex facilis ad quaerat nemo, veniam quibusdam similique
                            Ipsum, aliquid?</p>
                        <i><FaFacebook size="2em" color="white" /></i>
                        <i><FaTwitter size="2em" color="white" /></i>
                        <i><FaTelegram size="2em" color="white" /></i>
                        <i><FaLinkedin size="2em" color="white" /></i>
                    </div>
                    <div className="col-md-4">
                        <h6>quick links</h6>
                        <hr />
                        <div><Link to="/">Home</Link></div>
                        <div><Link to="/about">About Us</Link></div>
                        <div><Link to="/contact">Services</Link></div>
                        <div><Link to="/contact">Testimonials</Link></div>
                        <div><Link to="/contact">Pricing</Link></div>
                        <div><Link to="/contact">Contact Us</Link></div>
                    </div>
                    <div className="col-md-4">
                        <h6>Contact Info</h6>
                        <hr />
                        <div><p className="text-white mb-1">Nepalgunj Banke,NEPAL</p></div>
                        <div ><p className="text-white mb-1"> +977-123456789</p></div>
                        <div ><p className="text-white mb-1"> +977-987654321</p></div>
                        <div ><p className="text-white mb-1"> fitfactory@gmail.com</p></div>



                    </div>

                </div>

            </div>

        </section>
    );
}
export default Footer;