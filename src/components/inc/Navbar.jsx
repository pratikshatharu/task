import React from 'react';
import { Link } from "react-router-dom";
import './navbar.css';

const Navbar = () => {
    return (
        <>
            {/* <nav className="navbar navbar-expand-lg navbar-main bg-dark">
                <NavLink className="navbar-brand navbar-logo" to="/" exact>
                    FitFactory
                </NavLink>
                <button className="navbar-toggler" type="button"
                    data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupporredContent"
                    aria-expanded="false"
                    aria-label="Toggle navigation" >


                </button>

                <div
                    className="collapse navbar-collapse"
                    id="navbarSupporredContent">
                    <ul className="navbar-nav ml-auto">
                        <div className="hori-selector">
                            <div className="left"></div>
                            <div className="right"></div>
                        </div>
                        <li className="nav-item text-white active">
                            <NavLink className="link" to="/" exact>
                                <i>HOME</i>
                            </NavLink>

                        </li>

                        <li className="nav-item text-white ">

                            <NavLink className="link" to="/" >
                                <i>ABOUT</i>
                            </NavLink>

                        </li>

                        <li className="nav-item text-white">
                            <NavLink className="link" to="/" >
                                <i>TRAINERS</i>
                            </NavLink>

                        </li>

                        <li className="nav-item text-white">
                            <NavLink className="link" to="/" >
                                <i>TESTIMONIALS</i>
                            </NavLink>

                        </li>

                        <li className="nav-item text-white">
                            <NavLink className="link" to="/" >
                                <i>PRICING</i>
                            </NavLink>

                        </li>

                        <li className="nav-item text-white">
                            <NavLink className="link" to="/" >
                                <i>CONTACT</i>
                            </NavLink>

                        </li>
                    </ul>
                </div>
            </nav>*/}
            <nav className="navbar navbar-expand-md navbar-dark bg-dark shadow">
                <div className="container-fluid">

                    <Link to="/" className="navbar-brand" >Fit <span className="text-danger">factory</span></Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">

                                <Link to="/" className="nav-link " >HOME</Link>
                            </li>
                            <li className="nav-item">

                                <Link to="/about" className="nav-link  " >ABOUT</Link>
                            </li>
                            <li className="nav-item">

                                <Link to="/trainers" className="nav-link " >TRAINERS</Link>
                            </li>
                            <li className="nav-item">

                                <Link to="/testimonials" className="nav-link " >TESTIMONIALS</Link>
                            </li>
                            <li className="nav-item">

                                <Link to="/price" className="nav-link " >PRICING</Link>
                            </li>
                            <li className="nav-item">

                                <Link to="/contact" className="nav-link " >CONTACT</Link>
                            </li>

                        </ul>
                        <form className="d-flex">
                            <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                            <button className="btn btn-outline-success" type="submit">Search</button>
                        </form>
                    </div>
                </div>
            </nav>




        </>
    )
}


export default Navbar;