import React from 'react';
import gym8 from '../tasbir/gym8.jpg';
import gym9 from '../tasbir/gym9.jpg';
import gym7 from '../tasbir/gym7.jpg';
import { Link } from "react-router-dom";



function Slider6() {
    return (
        <>
            <div class="row">
                <div class="col-md-12">

                    <div id="carouselExampleDark" class="carousel carousel-dark fadded" data-bs-ride="carousel">

                        <div class="carousel-inner" >
                            <div class="carousel-item active" data-bs-interval="1000">
                                <img src={gym8} class="d-block w-100 mx-auto" alt="poster" />
                                <div class="carousel-caption d-none d-md-block">
                                    <h1 className="text-dark">Our services</h1>
                                    <h5 className="text-dark">Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas, aut rem natus sit ex dolor.</h5>
                                    <Link to="/about" className="btn btn-danger shadow">Read More!</Link>
                                </div>
                            </div>
                            <div class="carousel-item" data-bs-interval="1000">
                                <img src={gym9} class="d-block w-100 mx-auto opacity=0.3rem" alt="poster" />
                                <div class="carousel-caption d-none d-md-block">
                                    <h1 className="text-white">Our Services</h1>
                                    <h5 className="text-white">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dicta, hic asperiores velit debitis tempora aliquid.</h5>
                                    <Link to="/about" className="btn btn-danger shadow">Read More!</Link>
                                </div>
                            </div>
                            <div class="carousel-item" data-bs-interval="1000">
                                <img src={gym7} class="d-block w-100 mx-auto opacity=0.3rem" alt="poster" />
                                <div class="carousel-caption d-none d-md-block">
                                    <h1 className="text-white">Our Services</h1>
                                    <h5 className="text-white">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio at impedit non totam fugit possimus!</h5>
                                    <Link to="/about" className="btn btn-danger shadow">Read More!</Link>
                                </div>

                            </div>

                        </div>

                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>

                </div>
            </div>
        </>
    );
}


export default Slider6;