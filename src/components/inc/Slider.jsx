import React from 'react';
import slider4 from '../tasbir/slider4.jpg';
import { Link } from "react-router-dom";
import "./slider.css";


function Slider() {
    return (
        <>
            <div class="row">
                <div class="col-md-12">
                    <div id="carouselExampleDark" class="carousel carousel-dark carousel-fade" data-bs-ride="carousel">
                        <div class="carousel-inner" >
                            <div class="carousel-item active" data-bs-interval="2000">
                                <img src={slider4} class="d-block w-100 my-auto img-fluid " alt="slider" />
                                <div class="carousel-caption  header-heading d-none d-md-block d-sm-block d-xsm-block ">
                                    <h1 className=" text-white ">DON'T DECREASE YOUR GOAL.</h1>
                                    <h2 className="text-white "> INCREASE THE EFFORT.</h2>
                                    <p className="text-white">Let your soul glow</p>
                                    <Link to="/contact" className="btn btn-warning shadow ">Get Started</Link>
                                    <Link to="/about" className="btn btn-secondary">Read More!</Link>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </>
    );
}


export default Slider;



