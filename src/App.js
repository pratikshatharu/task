import React from 'react';
import './App.css';
import Price from './components/pages/Price';
import About from "./components/pages/About";
import Contact from './components/pages/Contact';
import Trainers from './components/pages/Trainers';
import Testimonials from './components/pages/Testimonials';
import Home from "./components/pages/Home";
import Navbar from "./components/inc/Navbar";
import {BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Footer from "./components/inc/Footer";

function App() {
  return (
    <Router>
    <>

    <Navbar/>
    <Switch>
    <Route exact path="/">
      <Home/>
    </Route>

    <Route  path="/about">
      <About/>
    </Route>
    <Route  path="/Trainers">
      <Trainers/>
    </Route>
    <Route  path="/Testimonials">
      <Testimonials/>
    </Route>
    <Route  path="/Price">
      <Price/>
    </Route>
    <Route  path="/contact">
      <Contact/>
    </Route>
    </Switch>
   
    <Footer/>
 </>
 </Router>
 

  );
  
}

export default App;
